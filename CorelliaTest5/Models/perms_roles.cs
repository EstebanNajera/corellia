namespace CorelliaTest5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class perms_roles
    {
        [Key]
        public int id_PR { get; set; }

        public int role_id_PR { get; set; }

        public int perm_id_PR { get; set; }

        public virtual perms perms { get; set; }

        public virtual roles roles { get; set; }
    }
}
