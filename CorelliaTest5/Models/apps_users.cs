namespace CorelliaTest5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class apps_users
    {
        [Key]
        public int id_AU { get; set; }

        public int user_id_AU { get; set; }

        public int app_id_AU { get; set; }

        public virtual apps apps { get; set; }

        public virtual users users { get; set; }
    }
}
