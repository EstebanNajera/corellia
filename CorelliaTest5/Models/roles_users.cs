namespace CorelliaTest5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class roles_users
    {
        [Key]
        public int id_RU { get; set; }

        public int user_id_RU { get; set; }

        public int role_id_RU { get; set; }

        public virtual roles roles { get; set; }

        public virtual users users { get; set; }
    }
}
