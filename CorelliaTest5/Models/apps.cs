namespace CorelliaTest5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class apps
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public apps()
        {
            apps_users = new HashSet<apps_users>();
            perms = new HashSet<perms>();
            roles = new HashSet<roles>();
        }

        [Key]
        public int IdAPP { get; set; }

        [StringLength(255)]
        public string id_app { get; set; }

        [StringLength(255)]
        public string app_name { get; set; }

        [StringLength(255)]
        public string app_description { get; set; }

        public bool? app_status { get; set; }

        public int? app_id_tenant { get; set; }

        public DateTime? APP_created_at { get; set; }

        public DateTime? APP_updated_at { get; set; }

        public virtual Tenants Tenants { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<apps_users> apps_users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<perms> perms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<roles> roles { get; set; }
    }
}
