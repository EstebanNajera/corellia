namespace CorelliaTest5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sessions
    {
        public int id { get; set; }

        [StringLength(255)]
        public string sessionKey { get; set; }

        [StringLength(255)]
        public string username { get; set; }

        [StringLength(255)]
        public string id_tenant { get; set; }

        [StringLength(255)]
        public string perms { get; set; }

        public int? timeout { get; set; }

        public DateTime? dateCreated { get; set; }

        public DateTime? lastUpdate { get; set; }
    }
}
