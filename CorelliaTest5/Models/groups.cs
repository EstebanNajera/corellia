namespace CorelliaTest5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class groups
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public groups()
        {
            groups_users = new HashSet<groups_users>();
        }

        [Key]
        public int Id_Groups { get; set; }

        [StringLength(255)]
        public string group_name { get; set; }

        [StringLength(255)]
        public string group_description { get; set; }

        public bool? group_status { get; set; }

        public int? group_id_tenant { get; set; }

        public DateTime? Group_created_at { get; set; }

        public DateTime? Group_updated_at { get; set; }

        public virtual Tenants Tenants { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<groups_users> groups_users { get; set; }
    }
}
