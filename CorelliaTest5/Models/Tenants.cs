namespace CorelliaTest5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Tenants
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tenants()
        {
            apps = new HashSet<apps>();
            groups = new HashSet<groups>();
        }

        [Key]
        public int IdTenant { get; set; }

        [StringLength(255)]
        public string id_tenant { get; set; }

        [StringLength(255)]
        public string tenant_name { get; set; }

        [StringLength(255)]
        public string Tenant_description { get; set; }

        public bool? tenant_status { get; set; }

        public DateTime? tenant_created_at { get; set; }

        public DateTime? tenant_updated_at { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<apps> apps { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<groups> groups { get; set; }
    }
}
