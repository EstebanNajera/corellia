namespace CorelliaTest5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class roles
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public roles()
        {
            perms_roles = new HashSet<perms_roles>();
            roles_users = new HashSet<roles_users>();
        }

        [Key]
        public int Id_role { get; set; }

        [StringLength(255)]
        public string role_name { get; set; }

        [StringLength(255)]
        public string role_description { get; set; }

        public bool? role_status { get; set; }

        public int? role_id_app { get; set; }

        public DateTime? role_created_at { get; set; }

        public DateTime? role_updated_t { get; set; }

        public virtual apps apps { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<perms_roles> perms_roles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<roles_users> roles_users { get; set; }
    }
}
