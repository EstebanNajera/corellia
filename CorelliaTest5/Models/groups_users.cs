namespace CorelliaTest5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class groups_users
    {
        [Key]
        public int id_GU { get; set; }

        public int group_id_GU { get; set; }

        public int user_id_GU { get; set; }

        public virtual groups groups { get; set; }

        public virtual users users { get; set; }
    }
}
