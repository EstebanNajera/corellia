namespace CorelliaTest5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class perms
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public perms()
        {
            perms_roles = new HashSet<perms_roles>();
        }

        [Key]
        public int Id_Perm { get; set; }

        [StringLength(255)]
        public string perm_name { get; set; }

        [StringLength(255)]
        public string perm_description { get; set; }

        public bool? perm_status { get; set; }

        public int? perm_id_app { get; set; }

        public DateTime? perm_created_at { get; set; }

        public DateTime? perm_updated_at { get; set; }

        public virtual apps apps { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<perms_roles> perms_roles { get; set; }
    }
}
