namespace CorelliaTest5.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CorelliaModel : DbContext
    {
        public CorelliaModel()
            : base("name=Model")
        {
        }

        public virtual DbSet<apps> apps { get; set; }
        public virtual DbSet<apps_users> apps_users { get; set; }
        public virtual DbSet<groups> groups { get; set; }
        public virtual DbSet<groups_users> groups_users { get; set; }
        public virtual DbSet<perms> perms { get; set; }
        public virtual DbSet<perms_roles> perms_roles { get; set; }
        public virtual DbSet<roles> roles { get; set; }
        public virtual DbSet<roles_users> roles_users { get; set; }
        public virtual DbSet<sessions> sessions { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<Tenants> Tenants { get; set; }
        public virtual DbSet<users> users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<apps>()
                .Property(e => e.id_app)
                .IsUnicode(false);

            modelBuilder.Entity<apps>()
                .Property(e => e.app_name)
                .IsUnicode(false);

            modelBuilder.Entity<apps>()
                .Property(e => e.app_description)
                .IsUnicode(false);

            modelBuilder.Entity<apps>()
                .HasMany(e => e.apps_users)
                .WithRequired(e => e.apps)
                .HasForeignKey(e => e.app_id_AU)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<apps>()
                .HasMany(e => e.perms)
                .WithOptional(e => e.apps)
                .HasForeignKey(e => e.perm_id_app);

            modelBuilder.Entity<apps>()
                .HasMany(e => e.roles)
                .WithOptional(e => e.apps)
                .HasForeignKey(e => e.role_id_app);

            modelBuilder.Entity<groups>()
                .Property(e => e.group_name)
                .IsUnicode(false);

            modelBuilder.Entity<groups>()
                .Property(e => e.group_description)
                .IsUnicode(false);

            modelBuilder.Entity<groups>()
                .HasMany(e => e.groups_users)
                .WithRequired(e => e.groups)
                .HasForeignKey(e => e.group_id_GU)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<perms>()
                .Property(e => e.perm_name)
                .IsUnicode(false);

            modelBuilder.Entity<perms>()
                .Property(e => e.perm_description)
                .IsUnicode(false);

            modelBuilder.Entity<perms>()
                .HasMany(e => e.perms_roles)
                .WithRequired(e => e.perms)
                .HasForeignKey(e => e.perm_id_PR)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<roles>()
                .Property(e => e.role_name)
                .IsUnicode(false);

            modelBuilder.Entity<roles>()
                .Property(e => e.role_description)
                .IsUnicode(false);

            modelBuilder.Entity<roles>()
                .HasMany(e => e.perms_roles)
                .WithRequired(e => e.roles)
                .HasForeignKey(e => e.role_id_PR)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<roles>()
                .HasMany(e => e.roles_users)
                .WithRequired(e => e.roles)
                .HasForeignKey(e => e.role_id_RU)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sessions>()
                .Property(e => e.sessionKey)
                .IsUnicode(false);

            modelBuilder.Entity<sessions>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<sessions>()
                .Property(e => e.id_tenant)
                .IsUnicode(false);

            modelBuilder.Entity<sessions>()
                .Property(e => e.perms)
                .IsUnicode(false);

            modelBuilder.Entity<Tenants>()
                .Property(e => e.id_tenant)
                .IsUnicode(false);

            modelBuilder.Entity<Tenants>()
                .Property(e => e.tenant_name)
                .IsUnicode(false);

            modelBuilder.Entity<Tenants>()
                .Property(e => e.Tenant_description)
                .IsUnicode(false);

            modelBuilder.Entity<Tenants>()
                .HasMany(e => e.apps)
                .WithOptional(e => e.Tenants)
                .HasForeignKey(e => e.app_id_tenant);

            modelBuilder.Entity<Tenants>()
                .HasMany(e => e.groups)
                .WithOptional(e => e.Tenants)
                .HasForeignKey(e => e.group_id_tenant);

            modelBuilder.Entity<users>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.user_remember_token)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.apps_users)
                .WithRequired(e => e.users)
                .HasForeignKey(e => e.user_id_AU)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.groups_users)
                .WithRequired(e => e.users)
                .HasForeignKey(e => e.user_id_GU)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.roles_users)
                .WithRequired(e => e.users)
                .HasForeignKey(e => e.user_id_RU)
                .WillCascadeOnDelete(false);
        }
    }
}
