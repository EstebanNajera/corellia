﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CorelliaTest5.Models;

namespace CorelliaTest5.Controllers
{
    public class TenantsController : ApiController
    {
        private CorelliaModel db = new CorelliaModel();

        // GET: api/Tenants
        [Authorize]
        public IQueryable<Tenants> GetTenants()
        {
            return db.Tenants;
        }

        // GET: api/Tenants/5
        [ResponseType(typeof(Tenants))]
        public async Task<IHttpActionResult> GetTenants(int id)
        {
            Tenants tenants = await db.Tenants.FindAsync(id);
            if (tenants == null)
            {
                return NotFound();
            }

            return Ok(tenants);
        }

        // PUT: api/Tenants/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTenants(int id, Tenants tenants)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tenants.IdTenant)
            {
                return BadRequest();
            }

            db.Entry(tenants).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TenantsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tenants
        [ResponseType(typeof(Tenants))]
        public async Task<IHttpActionResult> PostTenants(Tenants tenants)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Tenants.Add(tenants);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = tenants.IdTenant }, tenants);
        }

        // DELETE: api/Tenants/5
        [ResponseType(typeof(Tenants))]
        public async Task<IHttpActionResult> DeleteTenants(int id)
        {
            Tenants tenants = await db.Tenants.FindAsync(id);
            //apps
            var TA = db.apps.Where(a => a.app_id_tenant == tenants.IdTenant).ToList();
            //permisos
            var AP = db.perms.Where(ap => ap.perm_id_app == ap.apps.IdAPP).ToList();
            var PR = db.perms_roles.Where(pr => pr.perm_id_PR == pr.perms.Id_Perm).ToList();
            //roles
            var RO = db.roles.Where(ro => ro.role_id_app == ro.apps.IdAPP).ToList();
            var RU = db.roles_users.Where(ru => ru.role_id_RU == ru.roles.Id_role).ToList();
            var AU = db.apps_users.Where(au => au.app_id_AU == au.apps.IdAPP).ToList();
            //grupos
            var TG = db.groups.Where(g => g.group_id_tenant == tenants.IdTenant).ToList();
            var GU = db.groups_users.Where(gu => gu.group_id_GU == gu.groups.Id_Groups).ToList();

            if (tenants == null)
            {
                return NotFound();
            }

            db.apps.RemoveRange(TA);
            db.perms.RemoveRange(AP);
            db.perms_roles.RemoveRange(PR);
            db.roles.RemoveRange(RO);
            db.roles_users.RemoveRange(RU);
            db.apps_users.RemoveRange(AU);
            db.groups.RemoveRange(TG);
            db.groups_users.RemoveRange(GU);
            db.Tenants.Remove(tenants);
            await db.SaveChangesAsync();

            return Ok(tenants);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TenantsExists(int id)
        {
            return db.Tenants.Count(e => e.IdTenant == id) > 0;
        }
    }
}