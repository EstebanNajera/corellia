﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CorelliaTest5.Models;

namespace CorelliaTest5.Controllers
{
    public class apps_usersController : ApiController
    {
        private CorelliaModel db = new CorelliaModel();

        // GET: api/apps_users
        public IQueryable<apps_users> Getapps_users()
        {
            return db.apps_users;
        }

        // GET: api/apps_users/5
        [ResponseType(typeof(apps_users))]
        public async Task<IHttpActionResult> Getapps_users(int id)
        {
            apps_users apps_users = await db.apps_users.FindAsync(id);
            if (apps_users == null)
            {
                return NotFound();
            }

            return Ok(apps_users);
        }

        // PUT: api/apps_users/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Putapps_users(int id, apps_users apps_users)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != apps_users.id_AU)
            {
                return BadRequest();
            }

            db.Entry(apps_users).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!apps_usersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/apps_users
        [ResponseType(typeof(apps_users))]
        public async Task<IHttpActionResult> Postapps_users(apps_users apps_users)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.apps_users.Add(apps_users);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = apps_users.id_AU }, apps_users);
        }

        // DELETE: api/apps_users/5
        [ResponseType(typeof(apps_users))]
        public async Task<IHttpActionResult> Deleteapps_users(int id)
        {
            apps_users apps_users = await db.apps_users.FindAsync(id);
            if (apps_users == null)
            {
                return NotFound();
            }

            db.apps_users.Remove(apps_users);
            await db.SaveChangesAsync();

            return Ok(apps_users);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool apps_usersExists(int id)
        {
            return db.apps_users.Count(e => e.id_AU == id) > 0;
        }
    }
}