﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CorelliaTest5.Models;

namespace CorelliaTest5.Controllers
{
    public class permsController : ApiController
    {
        private CorelliaModel db = new CorelliaModel();

        // GET: api/perms
        public IQueryable<perms> Getperms()
        {
            return db.perms;
        }

        // GET: api/perms/5
        [ResponseType(typeof(perms))]
        public async Task<IHttpActionResult> Getperms(int id)
        {
            perms perms = await db.perms.FindAsync(id);
            if (perms == null)
            {
                return NotFound();
            }

            return Ok(perms);
        }

        // PUT: api/perms/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Putperms(int id, perms perms)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != perms.Id_Perm)
            {
                return BadRequest();
            }

            db.Entry(perms).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!permsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/perms
        [ResponseType(typeof(perms))]
        public async Task<IHttpActionResult> Postperms(perms perms)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.perms.Add(perms);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = perms.Id_Perm }, perms);
        }

        // DELETE: api/perms/5
        [ResponseType(typeof(perms))]
        public async Task<IHttpActionResult> Deleteperms(int id)
        {
            perms perms = await db.perms.FindAsync(id);
            var PR = db.perms_roles.Where(r => r.perm_id_PR == perms.Id_Perm).ToList();
            if (perms == null)
            {
                return NotFound();
            }

            db.perms_roles.RemoveRange(PR);
            db.perms.Remove(perms);
            await db.SaveChangesAsync();

            return Ok(perms);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool permsExists(int id)
        {
            return db.perms.Count(e => e.Id_Perm == id) > 0;
        }
    }
}