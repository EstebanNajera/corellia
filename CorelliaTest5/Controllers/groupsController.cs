﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CorelliaTest5.Models;

namespace CorelliaTest5.Controllers
{
    public class groupsController : ApiController
    {
        private CorelliaModel db = new CorelliaModel();

        // GET: api/groups
        public IQueryable<groups> Getgroups()
        {
            return db.groups;
        }

        // GET: api/groups/5
        [ResponseType(typeof(groups))]
        public async Task<IHttpActionResult> Getgroups(int id)
        {
            groups groups = await db.groups.FindAsync(id);
            if (groups == null)
            {
                return NotFound();
            }

            return Ok(groups);
        }

        // PUT: api/groups/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Putgroups(int id, groups groups)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != groups.Id_Groups)
            {
                return BadRequest();
            }

            db.Entry(groups).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!groupsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/groups
        [ResponseType(typeof(groups))]
        public async Task<IHttpActionResult> Postgroups(groups groups)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.groups.Add(groups);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = groups.Id_Groups }, groups);
        }

        // DELETE: api/groups/5
        [ResponseType(typeof(groups))]
        public async Task<IHttpActionResult> Deletegroups(int id)
        {
            groups groups = await db.groups.FindAsync(id);
            var GU = db.groups_users.Where(u => u.group_id_GU == groups.Id_Groups).ToList();
            if (groups == null)
            {
                return NotFound();
            }

            db.groups_users.RemoveRange(GU);
            db.groups.Remove(groups);
            await db.SaveChangesAsync();

            return Ok(groups);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool groupsExists(int id)
        {
            return db.groups.Count(e => e.Id_Groups == id) > 0;
        }
    }
}