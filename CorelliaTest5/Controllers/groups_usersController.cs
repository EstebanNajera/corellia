﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CorelliaTest5.Models;

namespace CorelliaTest5.Controllers
{
    public class groups_usersController : ApiController
    {
        private CorelliaModel db = new CorelliaModel();

        // GET: api/groups_users
        public IQueryable<groups_users> Getgroups_users()
        {
            return db.groups_users;
        }

        // GET: api/groups_users/5
        [ResponseType(typeof(groups_users))]
        public async Task<IHttpActionResult> Getgroups_users(int id)
        {
            groups_users groups_users = await db.groups_users.FindAsync(id);
            if (groups_users == null)
            {
                return NotFound();
            }

            return Ok(groups_users);
        }

        // PUT: api/groups_users/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Putgroups_users(int id, groups_users groups_users)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != groups_users.id_GU)
            {
                return BadRequest();
            }

            db.Entry(groups_users).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!groups_usersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/groups_users
        [ResponseType(typeof(groups_users))]
        public async Task<IHttpActionResult> Postgroups_users(groups_users groups_users)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.groups_users.Add(groups_users);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = groups_users.id_GU }, groups_users);
        }

        // DELETE: api/groups_users/5
        [ResponseType(typeof(groups_users))]
        public async Task<IHttpActionResult> Deletegroups_users(int id)
        {
            groups_users groups_users = await db.groups_users.FindAsync(id);
            if (groups_users == null)
            {
                return NotFound();
            }

            db.groups_users.Remove(groups_users);
            await db.SaveChangesAsync();

            return Ok(groups_users);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool groups_usersExists(int id)
        {
            return db.groups_users.Count(e => e.id_GU == id) > 0;
        }
    }
}