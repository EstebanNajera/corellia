﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CorelliaTest5.Models;

namespace CorelliaTest5.Controllers
{
    public class usersController : ApiController
    {
        private CorelliaModel db = new CorelliaModel();

        // GET: api/users
        [Authorize]
        public IQueryable<users> Getusers()
        {
            return db.users;
        }

        // GET: api/users/5
        [ResponseType(typeof(users))]
        public async Task<IHttpActionResult> Getusers(int id)
        {
            users users = await db.users.FindAsync(id);
            if (users == null)
            {
                return NotFound();
            }

            return Ok(users);
        }

        // PUT: api/users/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Putusers(int id, users users)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != users.Id_User)
            {
                return BadRequest();
            }

            db.Entry(users).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!usersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/users
        [ResponseType(typeof(users))]
        public async Task<IHttpActionResult> Postusers(users users)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.users.Add(users);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = users.Id_User }, users);
        }

        // DELETE: api/users/5
        [ResponseType(typeof(users))]
        public async Task<IHttpActionResult> Deleteusers(int id)
        {
            users users = await db.users.FindAsync(id);
            var AU = db.apps_users.Where(a => a.user_id_AU == users.Id_User).ToList();
            var GU = db.groups_users.Where(g => g.user_id_GU == users.Id_User).ToList();
            var RU = db.roles_users.Where(u => u.user_id_RU == users.Id_User).ToList();
            if (users == null)
            {
                return NotFound();
            }

            db.apps_users.RemoveRange(AU);
            db.groups_users.RemoveRange(GU);
            db.roles_users.RemoveRange(RU);
            db.users.Remove(users);
            await db.SaveChangesAsync();

            return Ok(users);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool usersExists(int id)
        {
            return db.users.Count(e => e.Id_User == id) > 0;
        }
    }
}