﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CorelliaTest5.Models;

namespace CorelliaTest5.Controllers
{
    public class roles_usersController : ApiController
    {
        private CorelliaModel db = new CorelliaModel();

        // GET: api/roles_users
        public IQueryable<roles_users> Getroles_users()
        {
            return db.roles_users;
        }

        // GET: api/roles_users/5
        [ResponseType(typeof(roles_users))]
        public async Task<IHttpActionResult> Getroles_users(int id)
        {
            roles_users roles_users = await db.roles_users.FindAsync(id);
            if (roles_users == null)
            {
                return NotFound();
            }

            return Ok(roles_users);
        }

        // PUT: api/roles_users/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Putroles_users(int id, roles_users roles_users)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != roles_users.id_RU)
            {
                return BadRequest();
            }

            db.Entry(roles_users).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!roles_usersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/roles_users
        [ResponseType(typeof(roles_users))]
        public async Task<IHttpActionResult> Postroles_users(roles_users roles_users)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.roles_users.Add(roles_users);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = roles_users.id_RU }, roles_users);
        }

        // DELETE: api/roles_users/5
        [ResponseType(typeof(roles_users))]
        public async Task<IHttpActionResult> Deleteroles_users(int id)
        {
            roles_users roles_users = await db.roles_users.FindAsync(id);
            if (roles_users == null)
            {
                return NotFound();
            }

            db.roles_users.Remove(roles_users);
            await db.SaveChangesAsync();

            return Ok(roles_users);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool roles_usersExists(int id)
        {
            return db.roles_users.Count(e => e.id_RU == id) > 0;
        }
    }
}