﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CorelliaTest5.Models;

namespace CorelliaTest5.Controllers
{
    public class sessionsController : ApiController
    {
        private CorelliaModel db = new CorelliaModel();

        // GET: api/sessions
        public IQueryable<sessions> Getsessions()
        {
            return db.sessions;
        }

        // GET: api/sessions/5
        [ResponseType(typeof(sessions))]
        public async Task<IHttpActionResult> Getsessions(int id)
        {
            sessions sessions = await db.sessions.FindAsync(id);
            if (sessions == null)
            {
                return NotFound();
            }

            return Ok(sessions);
        }

        // PUT: api/sessions/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Putsessions(int id, sessions sessions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sessions.id)
            {
                return BadRequest();
            }

            db.Entry(sessions).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!sessionsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/sessions
        [ResponseType(typeof(sessions))]
        public async Task<IHttpActionResult> Postsessions(sessions sessions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.sessions.Add(sessions);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = sessions.id }, sessions);
        }

        // DELETE: api/sessions/5
        [ResponseType(typeof(sessions))]
        public async Task<IHttpActionResult> Deletesessions(int id)
        {
            sessions sessions = await db.sessions.FindAsync(id);
            if (sessions == null)
            {
                return NotFound();
            }

            db.sessions.Remove(sessions);
            await db.SaveChangesAsync();

            return Ok(sessions);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool sessionsExists(int id)
        {
            return db.sessions.Count(e => e.id == id) > 0;
        }
    }
}