﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CorelliaTest5.Models;

namespace CorelliaTest5.Controllers
{
    public class perms_rolesController : ApiController
    {
        private CorelliaModel db = new CorelliaModel();

        // GET: api/perms_roles
        public IQueryable<perms_roles> Getperms_roles()
        {
            return db.perms_roles;
        }

        // GET: api/perms_roles/5
        [ResponseType(typeof(perms_roles))]
        public async Task<IHttpActionResult> Getperms_roles(int id)
        {
            perms_roles perms_roles = await db.perms_roles.FindAsync(id);
            if (perms_roles == null)
            {
                return NotFound();
            }

            return Ok(perms_roles);
        }

        // PUT: api/perms_roles/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Putperms_roles(int id, perms_roles perms_roles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != perms_roles.id_PR)
            {
                return BadRequest();
            }

            db.Entry(perms_roles).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!perms_rolesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/perms_roles
        [ResponseType(typeof(perms_roles))]
        public async Task<IHttpActionResult> Postperms_roles(perms_roles perms_roles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.perms_roles.Add(perms_roles);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = perms_roles.id_PR }, perms_roles);
        }

        // DELETE: api/perms_roles/5
        [ResponseType(typeof(perms_roles))]
        public async Task<IHttpActionResult> Deleteperms_roles(int id)
        {
            perms_roles perms_roles = await db.perms_roles.FindAsync(id);
            if (perms_roles == null)
            {
                return NotFound();
            }

            db.perms_roles.Remove(perms_roles);
            await db.SaveChangesAsync();

            return Ok(perms_roles);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool perms_rolesExists(int id)
        {
            return db.perms_roles.Count(e => e.id_PR == id) > 0;
        }
    }
}