﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CorelliaTest5.Models;

namespace CorelliaTest5.Controllers
{
    public class appsController : ApiController
    {
        private CorelliaModel db = new CorelliaModel();

        // GET: api/apps
        public IQueryable<apps> Getapps()
        {
            return db.apps;
        }

        // GET: api/apps/5
        [ResponseType(typeof(apps))]
        public async Task<IHttpActionResult> Getapps(int id)
        {
            apps apps = await db.apps.FindAsync(id);
            if (apps == null)
            {
                return NotFound();
            }

            return Ok(apps);
        }

        // PUT: api/apps/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Putapps(int id, apps apps)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != apps.IdAPP)
            {
                return BadRequest();
            }

            db.Entry(apps).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!appsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/apps
        [ResponseType(typeof(apps))]
        public async Task<IHttpActionResult> Postapps(apps apps)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.apps.Add(apps);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = apps.IdAPP }, apps);
        }

        // DELETE: api/apps/5
        [ResponseType(typeof(apps))]
        public async Task<IHttpActionResult> Deleteapps(int id)
        {
            apps apps = await db.apps.FindAsync(id);
            var AP = db.perms.Where(p => p.perm_id_app == apps.IdAPP).ToList();
            var AR = db.roles.Where(r => r.role_id_app == apps.IdAPP).ToList();
            var AU = db.apps_users.Where(u => u.app_id_AU == apps.IdAPP).ToList();
            var PR = db.perms_roles.Where(pr => pr.perm_id_PR == pr.perms.Id_Perm).ToList();
            var PRR = db.perms_roles.Where(prr => prr.role_id_PR == prr.roles.Id_role).ToList();
            var RU = db.roles_users.Where(ru => ru.role_id_RU == ru.roles.Id_role).ToList();
            if (apps == null)
            {
                return NotFound();
            }

            db.perms.RemoveRange(AP);
            db.roles.RemoveRange(AR);
            db.apps_users.RemoveRange(AU);
            db.perms_roles.RemoveRange(PR);
            db.perms_roles.RemoveRange(PRR);
            db.roles_users.RemoveRange(RU);
            db.apps.Remove(apps);
            await db.SaveChangesAsync();

            return Ok(apps);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool appsExists(int id)
        {
            return db.apps.Count(e => e.IdAPP == id) > 0;
        }
    }
}